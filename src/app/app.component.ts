import { Component } from '@angular/core';
import { IModel, IBasic } from './models/model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  charge = false;
  file: any;
  state: {
    province: IModel,
    department: IModel,
    district: IModel
  };
  province: IBasic[] = [];
  depart: IBasic[] = [];
  department: IBasic[] = [];
  district: IBasic[] = [];
  dist: IBasic[] = [];

  constructor(private toastr: ToastrService) {}

  fileChanged(e: any) {
    this.file = e.target.files[0];
    if (this.file.type !== 'text/plain') {
      this.toastr.error('Sorry, this type file is not valid');
    } else {
      this.toastr.success('Successfully updated');
      this.uploadDocument(this.file);
    }
  }
  uploadDocument(file: any) {
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
    const content = [...new Set((fileReader.result as string).split('\n'))];
    this.formatString(content);
    };
    fileReader.readAsText(this.file);
  }
  formatString(array: any) {
    // tslint:disable-next-line:forin
    for (const i in array) {
      const auxA = array[i].split('/');
      const provinceValues = this.detectProvince(auxA[0]);
      const departmentValues = this.detectValues(auxA[1]);
      const districtValues = this.detectValues(auxA[2]);
      this.state = {
        province : provinceValues,
        department: departmentValues,
        district: districtValues
      };
      this.fileProvince(this.state);
      this.fileDepartment(this.state);
      this.fileDistrict(this.state);
      this.charge = true;
    }
  }
  fileDepartment(data: any) {
    if (data.department && data.department.cod !== '') {
      this.department = this.checkProvince(data);
    }
  }
  fileDistrict(data: any) {
    if (data.district && data.district.cod !== '') {
      this.district = this.checkDistrict(data);
    }
  }
  fileProvince(data: any) {
    if (data.province && data.province.cod !== '') {
    this.province.push(data.province);
    this.province = this.getUnique(this.province, 'cod');
    }
  }
  checkDistrict(data: any) {
    for (const i in this.department) {
      if (this.department[i].cod === data.department.cod) {
        this.dist.push({
          father: this.department[i].cod,
          fatherDescription: this.department[i].name,
          cod: data.district.cod,
          name: data.district.name
        });
      }
    }
    return this.getUnique(this.dist, 'cod');
  }
  checkProvince(data: any) {
    for (const i in this.province) {
      if (this.province[i].cod === data.province.cod) {
        this.depart.push({
          father: this.province[i].cod,
          fatherDescription: this.province[i].name,
          cod: data.department.cod,
          name: data.department.name
        });
      }
    }
    return this.getUnique(this.depart, 'cod');
  }
  detectProvince(data: any) {
    const province = data.substring(1);
    const value = this.detectValues(province);
    return value;
  }
  detectValues(data: any): any {
    if (typeof (data) !== 'undefined') {
      const dataArray = data.trimLeft();
      const cod = dataArray.substr(0, dataArray.indexOf(' '));
      const name = dataArray.substr(dataArray.indexOf(' ') + 1).replace('\"', '');
      const values = {
        name,
        cod
      };
      return values;
    }
  }
  getUnique(arr, comp) {
    const unique = arr.map(e => e[comp]).map((e, i, final) => final.indexOf(e) === i && i).filter(e => arr[e]).map(e => arr[e]);
    return unique;
  }
}
