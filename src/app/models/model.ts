export interface IModel {
  name: string;
  cod: string;
}

export interface IBasic {
  father: string;
  fatherDescription: string;
  cod: string;
  name: string;
}
