# ReadFile

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.3.

# Install
Run npm install to download all the dependencies.

# File 

```
Your your should upload a text file, to see the table with the information, like this.
"01 Lima /  / "
"01 Lima / 50 Lima / "
"01 Lima / 51 Barranca / "
"01 Lima / 50 Lima / 201 San Martin de Porres"
"01 Lima / 50 Lima / 202 La Molina"
"01 Lima / 50 Lima / 203 San Isidro"
"02 Arequipa /  / "
"02 Arequipa / 63 Arequipa / "
"02 Arequipa / 64 Caylloma / "
"02 Arequipa / 63 Arequipa / 267 Cercado"
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
